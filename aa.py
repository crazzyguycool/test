# Secrets in comments
# AWS_SECRET_ACCESS_KEY=AKIAIOSFODNN7EXAMPLE
# SECRET_KEY_BASE=3b7cd727ee24e8444053437c36cc66c3
# PRIVATE_KEY="-----BEGIN RSA PRIVATE KEY-----\n...\n-----END RSA PRIVATE KEY-----"

def main():
    # Secrets in strings
    secret_key = "sk_test_4eC39HqLyjWDarjtT1zdp7dc"
    password = "mypassword123"

    # Secrets in function calls
    connect_to_database("username", "password")

    # Secrets in complex data structures
    config = {
        "client_secret": "e5e9fa1ba31ecd1ae84f75caaa474f3a663f05f4",
        "client_id": "Iv1.8f2e6e4a3b7c2c3a",
        "app_secret": "w7Uocj7n1K8B03xZLt_yP_8kK2rZ6-5qOP572GJg"
    }

    # Secrets in function definitions
    def set_password(new_password="anotherpassword"):
        pass
